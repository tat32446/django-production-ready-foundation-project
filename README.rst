Create a virtualenv:

$ python3.7 -m venv <virtual env path>
Activate the virtualenv you have just created:

$ source <virtual env path>/bin/activate
Install development requirements:

$ pip install -r requirements/local.txt


python manage.py migrate


python manage.py runserver 0.0.0.0:8000




----best practices ------------------------
Each app will have these in their folder:

views.py
models.py
others optionals (admin.py...)
So, you have :

Project
-- manage.py
-- Project
-- -- views.py
-- -- models.py
-- -- others
-- -- APP1
-- -- -- views.py
-- -- -- models.py
-- -- -- others
-- -- APP2
-- -- -- views.py
-- -- -- models.py
-- -- -- others
-- -- APPX
-- -- -- views.py
-- -- -- models.py
-- -- -- others

----------------------------------------------





.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


:License: MIT


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy my_awesome_project

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~





